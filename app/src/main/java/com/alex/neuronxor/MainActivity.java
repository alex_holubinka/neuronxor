package com.alex.neuronxor;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.alex.neuronxor.UserData.CWeights;
import com.alex.neuronxor.UserData.NeuralNetwork;
import com.alex.neuronxor.UserData.SerializeManager;

public class MainActivity extends AppCompatActivity {
    NeuralNetwork nn;
    int maxRuns = 50000;
    double minErrorCondition = 0.001;
    EditText x1;
    EditText x2;
    EditText out;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        x1 = (EditText) findViewById(R.id.input_neuron1);
        x2 = (EditText) findViewById(R.id.input_neuron2);
        out = (EditText) findViewById(R.id.out);



    }

    public void onClickTeach(View view) {
        nn = new NeuralNetwork(2, 4, 1);

        nn.run(maxRuns, minErrorCondition);
        CWeights weights = nn.getAllWeights();
        SerializeManager.serialise(this, weights, "NeuronWieghts");
    }

    public void onClickRun(View view) {
        double input[];
        input = new double[2];
        CWeights weightsout = new CWeights();
        weightsout = SerializeManager.deserialise(this, "NeuronWieghts");
        nn.weightUpdate = weightsout.weightUpdate;
        out.setText(String.valueOf(4));
        input[0] = 1.0;
        input[0] = Double.parseDouble(x1.getText().toString());
        input[1] = Double.parseDouble(x2.getText().toString());
        double outs = nn.runFromInput(input, maxRuns, minErrorCondition);
        out.setText(String.valueOf(outs));
}
}
